package com.qinsung.common.base;

import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;

/**
 * 基础逻辑层接口
 * @param <T>
 */
public interface IBaseService<T> {

    /**
     * 新增
     * @param t
     * @return
     */
    int add(T t);

    /**
     * 批量添加
     * @param list
     */
    void addAll(List<T> list);

    /**
     * 逻辑删除
     * @param id
     */
    void logicDelete(Integer id);

    /**
     * 删除
     * @param id
     */
    void delete(Integer id);

    /**
     * 详情
     * @param id
     * @return
     */
    T queryById(Integer id);

    /**
     * 更新
     * @param t
     */
    void update(T t);

    /**
     * 分页
     * @param request
     * @return
     */
    Page<T> queryPage(BaseRequest request);
}
