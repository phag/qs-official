package com.qinsung.common.base;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 基础静态类
 */
public abstract class AbstractController<S extends IBaseService, T extends BaseModel> extends BaseController {

    @Autowired
    protected S baseService;

    /**
     * 添加
     * @param t
     * @return
     */
    public R add(T t){
        return new R(baseService.add(t));
    }

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    public R logicRemove(Integer id){
        baseService.logicDelete(id);
        return new R();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    public R remove(Integer id){
        baseService.delete(id);
        return new R();
    }

    /**
     * 编辑
     * @param t
     * @return
     */
    public R edit(T t){
        baseService.update(t);
        return new R();
    }

    /**
     * 详情
     * @param id
     * @return
     */
    public R detail(Integer id){
        return new R(baseService.queryById(id));
    }

    /**
     * 分页
     * @param request
     * @return
     */
    public R page(BaseRequest request){
        return new R(baseService.queryPage(request));
    }
}
