package com.qinsung.common.base;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.qinsung.common.enums.CoreExceptionEnum;
import com.qinsung.common.enums.DelFlagEnum;
import com.qinsung.common.exception.CoreBusinessException;
import com.qinsung.common.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 基础逻辑层服务实现类
 */
@Service
public class BaseServiceImpl<M extends BaseMapper<T>, T extends BaseModel> extends ServiceImpl<M, T> implements IBaseService<T> {

    /**
     * 添加
     * @param t
     * @return
     */
    @Override
    @Transactional(rollbackFor = Throwable.class, timeout = 60)
    public int add(T t) {
        if(t == null){
            throw CoreBusinessException.throwException(CoreExceptionEnum.DATA_IS_NULL);
        }
        this.completeRecord(t);
        return baseMapper.insert(t);
    }

    /**
     * 批量添加
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Throwable.class, timeout = 60)
    public void addAll(List<T> list) {
        if(CollectionUtils.isEmpty(list)){
            return;
        }
        list.forEach(this::completeRecord);
        CollectionUtils.forEach(list, t -> {
            baseMapper.insert(t);
        });
    }

    /**
     * 逻辑删除
     * @param id
     */
    @Override
    @Transactional(rollbackFor = Throwable.class, timeout = 60)
    public void logicDelete(Integer id){
        T t = this.queryById(id);
        t.setUpdateTime(new Date());
        t.setDelFlag(DelFlagEnum.DELETE.getCode());
        baseMapper.updateById(t);
    }

    /**
     * 删除
     * @param id
     */
    @Override
    public void delete(Integer id) {
        baseMapper.deleteById(id);
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @Override
    public T queryById(Integer id){
        T t = baseMapper.selectById(id);
        if(null == t){
            throw CoreBusinessException.throwException(CoreExceptionEnum.DATA_NOT_EXIST);
        }
        if(DelFlagEnum.DELETE.getCode().equals(t.getDelFlag())){
            throw CoreBusinessException.throwException(CoreExceptionEnum.DATA_NOT_EXIST);
        }
        return t;
    }

    /**
     * 更新
     * @param t
     */
    @Override
    public void update(T t) {
        if(null == t){
            throw CoreBusinessException.throwException(CoreExceptionEnum.DATA_IS_NULL);
        }
        T t1 = this.queryById(t.getId());
        if(null == t1){
            throw CoreBusinessException.throwException(CoreExceptionEnum.DATA_NOT_EXIST);
        }
        t.setUpdateTime(new Date());
        baseMapper.updateById(t);
    }

    /**
     * 分页
     * @param request
     * @return
     */
    @Override
    public Page<T> queryPage(BaseRequest request){
        if(null == request.getPageNo() || request.getPageNo() <= 0){
            request.setPageNo(0);
        }
        if(null == request.getPageSize() || request.getPageSize() <= 0){
            request.setPageNo(10);
        }
        if(null == request.getOrderBy() || "".equals(request.getOrderBy())){
            request.setOrderBy(BaseTable.id);
            request.setAsc(true);
        }
        Page<T> page = new Page(request.getPageNo(), request.getPageSize(), request.getOrderBy(), request.isAsc());
        Wrapper<T> wrapper = new EntityWrapper<>();
        wrapper.eq(BaseTable.del_flag, DelFlagEnum.NORMAL.getCode());
        List<T> ts = baseMapper.selectPage(page, wrapper);
        page.setRecords(ts);
        return page;
    }

    /**
     * 填补基础数据
     * @param t
     */
    public void completeRecord(T t){
        if(t != null){
            Date date = new Date();
            if(null == t.getCreateTime()){
                t.setCreateTime(date);
            }
            if(null == t.getUpdateTime()){
                t.setUpdateTime(date);
            }
            if(null == t.getDelFlag()){
                t.setDelFlag(DelFlagEnum.NORMAL.getCode());
            }
        }
    }

    /**
     * 重写mybatis-plus的获取Page的方法
     * @param pageNo
     * @param pageSize
     * @param orderBy
     * @param isAsc
     * @return
     */
    public Page getPage(Integer pageNo, Integer pageSize, String orderBy, Boolean isAsc){
        if(null == pageNo){
            pageNo = 1;
        }
        if(null == pageSize){
            pageSize = 10;
        }
        if(StringUtils.isEmpty(orderBy)){
            orderBy = BaseTable.id;
        }
        if(null == isAsc){
            isAsc = true;
        }
        Page page = new Page(pageNo, pageSize, orderBy, isAsc);
        return page;
    }
}
