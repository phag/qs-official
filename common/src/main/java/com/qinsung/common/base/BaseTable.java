package com.qinsung.common.base;

/**
 * 基础字段接口
 */
public interface BaseTable {

    String id = "id";

    String create_time = "create_time";

    String update_time = "update_time";

    String del_flag = "del_flag";
}
