package com.qinsung.common.base;


/**
 * 基础dao层接口
 * @param <T>
 */
public interface BaseMapper<T> extends com.baomidou.mybatisplus.mapper.BaseMapper<T> {
}
