package com.qinsung.common.base;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * 基础请求模型类
 */
@Setter
@Getter
public class BaseRequest {

    /**
     * 当前页数
     */
    @NotNull
    private Integer pageNo;

    /**
     * 显示条数
     */
    @NotNull
    private Integer pageSize;

    /**
     * 关键词
     */
    private String keyword;

    /**
     * 排序字段
     */
    private String orderBy;

    /**
     * 是否顺序
     */
    private boolean isAsc = false;
}
