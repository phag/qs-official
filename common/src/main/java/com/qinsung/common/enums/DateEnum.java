package com.qinsung.common.enums;

public enum DateEnum {
    /**
     */
    YYYY("yyyy"),
    YYYYMM("yyyyMM"),
    YYYYMMDD("yyyyMMdd"),
    YYMMDDHHMMSS("yyMMddHHmmss"),
    YYYYMMDDHHMMSS("yyyyMMddHHmmss"),
    YYYYMMDDHHMM("yyyyMMddHHmm"),
    YYYYMMDDHHMMSSSSS("yyyyMMddHHmmssSSS"),
    YYYYMM_BYSEP("yyyy-MM"),
    YYYYMMDD_BYSEP("yyyy-MM-dd"),
    MMDD_BYSEP("MM-dd"),
    YYYYMMDDHHMMSS_BYSEP("yyyy-MM-dd HH:mm:ss"),
    YYYYMMDDHHMM_BYSEP("yyyy-MM-dd HH:mm"),
    YYYYMMDDHHMMSSSSS_BYSEP("yyyy-MM-dd HH:mm:ss.SSS"),
    HHMMSS("HHmmss"),
    HHMM_BYSEP("HH:mm"),
    HHMMSS_BYSEP("HH:mm:ss"),
    MINYEAR("1900"),
    MAXYEAR("9999"),
    MINDATE("19000101"),
    MAXDATE("99991231"),
    MINDATE_BYSEP("1900-01-01"),
    MAXDATE_BYSEP("9999-12-31");

    private String format = null;

    private DateEnum(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return this.format;
    }

    public String getValue() {
        return this.format;
    }
}
