package com.qinsung.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 自定义异常
 */
@Getter
@AllArgsConstructor
public enum CoreExceptionEnum {

    DATA_NOT_EXIST("10001", "数据不存在"),
    DATA_IS_NULL("10002", "数据为空"),
    FILE_IS_NULL("10003", "文件为空"),
    FILE_DOWNLOAD_ERROR("10004", "文件下载失败"),
    FILE_UPLOAD_ERROR("10005", "上传文件失败，请检查配置信息"),
    ;


    private String errorCode;
    private String errorMessage;
}
