package com.qinsung.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 逻辑删除枚举类
 */
@Getter
@AllArgsConstructor
public enum DelFlagEnum {

    NORMAL(0, "正常"),
    DELETE(1, "删除");

    private Integer code;
    private String desc;
}
