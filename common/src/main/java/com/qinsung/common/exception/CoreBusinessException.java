package com.qinsung.common.exception;

import com.qinsung.common.enums.CoreExceptionEnum;

/**
 * 自定义抛出异常
 */
public class CoreBusinessException extends RuntimeException{

    private String errorCode;
    private String errorMessage;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public CoreBusinessException(String code, String desc){
        super(desc);
        this.errorCode = code;
        this.errorMessage = desc;
    }

    public static CoreBusinessException throwException(CoreExceptionEnum coreExceptionEnum){
        return new CoreBusinessException(coreExceptionEnum.getErrorCode(), coreExceptionEnum.getErrorMessage());
    }
}
