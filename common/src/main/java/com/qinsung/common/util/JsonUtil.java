package com.qinsung.common.util;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public final class JsonUtil {
    private static ObjectMapper objectMapper = new CustomObjectMapper();

    private JsonUtil() {
    }

    public static <T> String toJson(T pojo) {
        try {
            String json = objectMapper.writeValueAsString(pojo);
            return json;
        } catch (Exception var3) {
            throw new RuntimeException(var3);
        }
    }

    public static <T> T fromJson(String json, Class<T> type) {
        try {
            T pojo = objectMapper.readValue(json, type);
            return pojo;
        } catch (Exception var4) {
            throw new RuntimeException(var4);
        }
    }

    public static <T> List<T> fromListJson(String json, Class<T> type) {
        Object pojo = Lists.newArrayList();

        try {
            JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, new Class[]{type});
            pojo = (List) objectMapper.readValue(json, javaType);
        } catch (IOException var4) {
            var4.printStackTrace();
        }

        return (List) pojo;
    }

    public static <K, T> Map<K, T> fromMapJson(String json, Class<K> keyType, Class<T> valueType) {
        Object pojo = Maps.newHashMap();

        try {
            JavaType javaType = objectMapper.getTypeFactory().constructParametricType(Map.class, new Class[]{keyType, valueType});
            pojo = (Map) objectMapper.readValue(json, javaType);
        } catch (IOException var5) {
            var5.printStackTrace();
        }

        return (Map) pojo;
    }

    static {
        objectMapper.setTimeZone(TimeZone.getTimeZone("GMT+8"));
    }

}
