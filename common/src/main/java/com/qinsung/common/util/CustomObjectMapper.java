package com.qinsung.common.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CustomObjectMapper extends ObjectMapper {
    private boolean camelCaseToLowerCaseWithUnderscores = false;
    private String dateFormatPattern;

    public void setCamelCaseToLowerCaseWithUnderscores(boolean camelCaseToLowerCaseWithUnderscores) {
        this.camelCaseToLowerCaseWithUnderscores = camelCaseToLowerCaseWithUnderscores;
    }

    public void setDateFormatPattern(String dateFormatPattern) {
        this.dateFormatPattern = dateFormatPattern;
    }

    public CustomObjectMapper() {
        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public CustomObjectMapper(boolean camelCaseToLowerCaseWithUnderscores) {
        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        if (camelCaseToLowerCaseWithUnderscores) {
            this.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        }

    }

    public void init() {
        this.configure(SerializationFeature.INDENT_OUTPUT, true);
        if (this.camelCaseToLowerCaseWithUnderscores) {
            this.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        }

        if (StringUtils.isNotEmpty(this.dateFormatPattern)) {
            DateFormat dateFormat = new SimpleDateFormat(this.dateFormatPattern);
            this.setDateFormat(dateFormat);
        }

        SimpleModule module = new CustomObjectMapper.CustomerModule();
        module.addSerializer(BigDecimal.class, new ToStringSerializer());
        this.registerModule(module);
    }

    class CustomerModule extends SimpleModule {
        public CustomerModule() {
            this.addDeserializer(String.class, new StdScalarDeserializer<String>(String.class) {
                public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
                    return StringUtils.trim(jp.getValueAsString());
                }
            });
        }
    }
}

