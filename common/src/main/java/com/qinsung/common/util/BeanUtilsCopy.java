package com.qinsung.common.util;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author jsonpro
 * @date 2018/3/19
 */
public class BeanUtilsCopy extends BeanUtils {

    public static <T> T copy(Object source, Class<T> t){
        T dest = null;
        try {
            dest = t.newInstance();
            BeanUtils.copyProperties(source, dest);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return dest;
    }

    public static <T> List<T> copyList(Collection sourceList, Class<T> t){
        if (CollectionUtils.isEmpty(sourceList)) {
            return Collections.EMPTY_LIST;
        }
        List<T> destinationList = Lists.newArrayList();
        for (Iterator iterator = sourceList.iterator(); iterator.hasNext(); ) {
            Object source = iterator.next();
            T dest = null;
            try {
                dest = t.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            BeanUtils.copyProperties(source, dest);
            destinationList.add(dest);
        }
        return destinationList;
    }

    public static <T> Page<T> copyPage(Page page, Class<T> t){
        Page<T> destPage = new Page<>();
        BeanUtils.copyProperties(page, destPage);
        List<T> destinationList = copyList(page.getRecords(), t);
        destPage.setRecords(destinationList);
        return destPage;
    }
}
