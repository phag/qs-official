package com.qinsung.dal.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 职位申请详情返回实体
 */
@Setter
@Getter
public class QsPostRequestDetail {

    /**
     * 申请id
     */
    private Integer id;

    /**
     * 职位招聘id
     */
    private Integer recruitId;

    /**
     * 简历
     */
    private String curriculumVitae;

    /**
     * 申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 岗位
     */
    private String post;
    /**
     * 薪资范围
     */
    private String salary;
    /**
     * 福利
     */
    private String welfare;
    /**
     * 要求
     */
    private String demand;
    /**
     * 职位描述
     */
    private String postDescription;
    /**
     * 任职要求
     */
    private String jobRequirements;

}
