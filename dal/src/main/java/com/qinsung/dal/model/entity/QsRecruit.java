package com.qinsung.dal.model.entity;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.qinsung.common.base.BaseModel;
import lombok.Data;

import java.io.Serializable;
    /**
 * <p>
 * 招聘信息表
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Data
@TableName(value = "qs_recruit")
public class QsRecruit extends BaseModel {

	/**
	 * 岗位
	 */
	private String post;
	/**
	 * 薪资范围
	 */
	private String salary;
	/**
	 * 福利
	 */
	private String welfare;
	/**
	 * 要求
	 */
	private String demand;
	/**
	 * 职位描述
	 */
	@TableField(value="post_description")
	private String postDescription;
	/**
	 * 任职要求
	 */
	@TableField(value="job_requirements")
	private String jobRequirements;

	@Override
	protected Serializable pkVal() {
		return getId();
	}

}
