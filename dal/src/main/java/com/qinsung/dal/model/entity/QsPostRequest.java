package com.qinsung.dal.model.entity;


import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.qinsung.common.base.BaseModel;
import lombok.Data;

import java.io.Serializable;
    /**
 * <p>
 * 职位申请表
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Data
@TableName(value = "qs_post_request")
public class QsPostRequest extends BaseModel {

	/**
	 * 申请职位
	 */
	private Integer recruitId;
	/**
	 * 简历文件地址
	 */
	@TableField(value="curriculum_vitae")
	private String curriculumVitae;

	@Override
	protected Serializable pkVal() {
		return getId();
	}

}
