package com.qinsung.dal.model.table;

/**
 * 招聘信息字段接口
 */
public interface QsRecruitTable {

    /**
     * 岗位
     */
    String post = "post";
    /**
     * 薪资范围
     */
    String salary = "salary";
    /**
     * 福利
     */
    String welfare = "welfare";
    /**
     * 要求
     */
    String demand = "demand";
    /**
     * 职位描述
     */
    String post_description = "post_description";
    /**
     * 任职要求
     */
    String job_requirements = "job_requirements";
}
