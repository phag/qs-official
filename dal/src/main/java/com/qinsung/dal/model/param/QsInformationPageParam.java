package com.qinsung.dal.model.param;

import lombok.Getter;
import lombok.Setter;

/**
 * 资讯分页参数
 */
@Setter
@Getter
public class QsInformationPageParam {

    /**
     * 资讯类型
     */
    private Integer type;

    /**
     * 当前页数
     */
    private Integer pageNo;

    /**
     * 显示条数
     */
    private Integer pageSize;

    /**
     * 关键词
     */
    private String keyword;

    /**
     * 排序字段
     */
    private String orderBy;

    /**
     * 是否顺序
     */
    private boolean isAsc = false;
}
