package com.qinsung.dal.model.table;

/**
 * 职位申请字段接口
 */
public interface QsPostRequestTable {

    /**
     * 申请职位
     */
    String post = "post";
    /**
     * 简历文件地址
     */
    String curriculum_vitae = "curriculum_vitae";
}
