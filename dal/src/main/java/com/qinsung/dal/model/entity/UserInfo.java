package com.qinsung.dal.model.entity;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.qinsung.common.base.BaseModel;
import lombok.Data;

import java.io.Serializable;
    /**
 * <p>
 * 
 * </p>
 *
 * @author GY
 * @since 2019-03-14
 */
@Data
@TableName("user_info")
public class UserInfo extends BaseModel {

	/**
	 * 用户名
	 */
	@TableField(value = "user_name")
	private String userName;
	/**
	 * 年龄
	 */
	@TableField(value = "user_age")
	private String userAge;

	@Override
	protected Serializable pkVal() {
		return getId();
	}

}
