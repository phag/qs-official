package com.qinsung.dal.model.entity;


import com.baomidou.mybatisplus.annotations.TableName;
import com.qinsung.common.base.BaseModel;
import lombok.Data;
import java.io.Serializable;
    /**
 * <p>
 * 资讯信息表
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Data
@TableName(value = "qs_information")
public class QsInformation extends BaseModel {

	/**
	 * 标题
	 */
	private String title;
	/**
	 * 图片
	 */
	private String image;

	/**
	 * 正文描述
	 */
	private String description;

	/**
	 * 文章
	 */
	private String article;
	/**
	 * 来源
	 */
	private String source;
	/**
	 * 类型（1行业动态、2政策法规、3今日热点）
	 */
	private Integer type;

	@Override
	protected Serializable pkVal() {
		return getId();
	}

}
