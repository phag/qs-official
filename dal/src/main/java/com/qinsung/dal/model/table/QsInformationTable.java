package com.qinsung.dal.model.table;

/**
 * 资讯信息字段接口
 */
public interface QsInformationTable {

    /**
     * 标题
     */
    String title = "title";
    /**
     * 图片
     */
    String image = "image";
    /**
     * 正文描述
     */
    String description = "description";
    /**
     * 文章
     */
    String article = "article";
    /**
     * 来源
     */
    String source = "source";
    /**
     * 类型（1行业动态、2政策法规、3今日热点）
     */
    String type = "type";
}
