package com.qinsung.dal.dao;

import com.qinsung.dal.model.entity.UserInfo;
import com.qinsung.common.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author GY
 * @since 2019-03-14
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}