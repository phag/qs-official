package com.qinsung.dal.dao;

import com.qinsung.dal.model.dto.QsPostRequestDetail;
import com.qinsung.dal.model.entity.QsPostRequest;
import com.qinsung.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
public interface QsPostRequestMapper extends BaseMapper<QsPostRequest> {

    /**
     * 获取职位申请详情
     * @param id
     * @return
     */
    QsPostRequestDetail detailById(@Param("id") Integer id);

}