package com.qinsung.dal.dao;

import com.qinsung.dal.model.entity.QsInformation;
import com.qinsung.common.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
public interface QsInformationMapper extends BaseMapper<QsInformation> {

}