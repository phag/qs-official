package com.qinsung.services.module;

import com.qinsung.dal.model.dto.QsPostRequestDetail;
import com.qinsung.dal.model.entity.QsPostRequest;
import com.qinsung.common.base.IBaseService;

/**
 * <p>
 * 职位申请表 服务类
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
public interface IQsPostRequestService extends IBaseService<QsPostRequest> {

    /**
     * 获取职位申请详情
     * @return
     */
    QsPostRequestDetail detailById(Integer id);
}
