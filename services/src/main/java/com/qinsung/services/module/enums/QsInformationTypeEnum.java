package com.qinsung.services.module.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 资讯类型枚举类
 */
@Getter
@AllArgsConstructor
public enum QsInformationTypeEnum {


    INDUSTRY_TRENDS(1, "行业动态"),
    POLICIES_REGULATIONS(2, "政策法规"),
    TODAY_HOT_SPOTS(3, "今日热点");

    private Integer code;

    private String desc;
}
