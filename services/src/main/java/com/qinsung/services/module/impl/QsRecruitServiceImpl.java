package com.qinsung.services.module.impl;

import com.qinsung.dal.dao.QsRecruitMapper;
import com.qinsung.dal.model.entity.QsRecruit;
import com.qinsung.services.module.IQsRecruitService;
import org.springframework.stereotype.Service;
import com.qinsung.common.base.BaseServiceImpl;

/**
 * <p>
 * 招聘信息表  服务实现类
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Service
public class QsRecruitServiceImpl extends BaseServiceImpl<QsRecruitMapper, QsRecruit> implements IQsRecruitService {

}