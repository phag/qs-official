package com.qinsung.services.module.impl;

import com.qinsung.dal.dao.QsPostRequestMapper;
import com.qinsung.dal.model.dto.QsPostRequestDetail;
import com.qinsung.dal.model.entity.QsPostRequest;
import com.qinsung.services.module.IQsPostRequestService;
import org.springframework.stereotype.Service;
import com.qinsung.common.base.BaseServiceImpl;

/**
 * <p>
 * 职位申请表  服务实现类
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Service
public class QsPostRequestServiceImpl extends BaseServiceImpl<QsPostRequestMapper, QsPostRequest> implements IQsPostRequestService {

    @Override
    public QsPostRequestDetail detailById(Integer id) {
        return baseMapper.detailById(id);
    }
}