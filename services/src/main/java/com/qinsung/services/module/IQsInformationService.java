package com.qinsung.services.module;

import com.baomidou.mybatisplus.plugins.Page;
import com.qinsung.dal.model.entity.QsInformation;
import com.qinsung.common.base.IBaseService;
import com.qinsung.dal.model.param.QsInformationPageParam;

/**
 * <p>
 * 资讯信息表 服务类
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
public interface IQsInformationService extends IBaseService<QsInformation> {

    /**
     * 资讯分页
     * @param param
     * @return
     */
    Page<QsInformation> queryPage(QsInformationPageParam param);
}
