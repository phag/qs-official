package com.qinsung.services.module.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.qinsung.common.base.BaseTable;
import com.qinsung.common.enums.DelFlagEnum;
import com.qinsung.dal.dao.QsInformationMapper;
import com.qinsung.dal.model.entity.QsInformation;
import com.qinsung.dal.model.param.QsInformationPageParam;
import com.qinsung.dal.model.table.QsInformationTable;
import com.qinsung.services.module.IQsInformationService;
import org.springframework.stereotype.Service;
import com.qinsung.common.base.BaseServiceImpl;

import java.util.List;

/**
 * <p>
 * 资讯信息表  服务实现类
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Service
public class QsInformationServiceImpl extends BaseServiceImpl<QsInformationMapper, QsInformation> implements IQsInformationService {


    /**
     * 资讯分页
     * @param param
     * @return
     */
    @Override
    public Page<QsInformation> queryPage(QsInformationPageParam param) {
        Page<QsInformation> page = getPage(param.getPageNo(), param.getPageSize(), param.getOrderBy(), param.isAsc());
        Wrapper<QsInformation> wrapper = new EntityWrapper<>();
        wrapper.eq(BaseTable.del_flag, DelFlagEnum.NORMAL.getCode());
        if(null != param.getType()){
            wrapper.eq(QsInformationTable.type, param.getType());
        }
        List<QsInformation> qsInformations = baseMapper.selectPage(page, wrapper);
        page.setRecords(qsInformations);
        return page;
    }

}