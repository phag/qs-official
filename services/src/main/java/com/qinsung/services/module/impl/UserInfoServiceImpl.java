package com.qinsung.services.module.impl;

import com.qinsung.dal.dao.UserInfoMapper;
import com.qinsung.dal.model.entity.UserInfo;
import com.qinsung.services.module.IUserInfoService;
import org.springframework.stereotype.Service;
import com.qinsung.common.base.BaseServiceImpl;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author GY
 * @since 2019-03-14
 */
@Service
public class UserInfoServiceImpl extends BaseServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

}