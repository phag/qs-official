package com.qinsung.services.module;

import com.qinsung.dal.model.entity.UserInfo;
import com.qinsung.common.base.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GY
 * @since 2019-03-14
 */
public interface IUserInfoService extends IBaseService<UserInfo> {
	
}
