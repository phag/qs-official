package com.qinsung.services.module;

import com.qinsung.dal.model.entity.QsRecruit;
import com.qinsung.common.base.IBaseService;

/**
 * <p>
 * 招聘信息表 服务类
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
public interface IQsRecruitService extends IBaseService<QsRecruit> {
	
}
