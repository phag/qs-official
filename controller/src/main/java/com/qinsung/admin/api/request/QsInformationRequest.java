package com.qinsung.admin.api.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 资讯请求实体
 */
@Setter
@Getter
public class QsInformationRequest {

    private Integer id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "图片")
    private String image;

    @ApiModelProperty(value = "正文描述")
    private String description;

    @ApiModelProperty(value = "文章")
    private String article;

    @ApiModelProperty(value = "来源")
    private String source;

    @ApiModelProperty(value = "类型")
    private Integer type;
}
