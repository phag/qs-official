package com.qinsung.admin.api.request;

import com.qinsung.common.base.BaseRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 资讯分页请求实体
 */
@Setter
@Getter
public class QsInformationPageRequest extends BaseRequest {

    @ApiModelProperty(value = "资讯类型")
    private Integer type;
}
