package com.qinsung.admin.api.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.qinsung.admin.api.request.QsPostAddRequest;
import com.qinsung.common.base.BaseRequest;
import com.qinsung.common.base.R;
import com.qinsung.dal.model.dto.QsPostRequestDetail;
import com.qinsung.dal.model.entity.QsPostRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import com.qinsung.services.module.IQsPostRequestService;
import com.qinsung.common.base.AbstractController;

import javax.validation.Valid;

/**
 * <p>
 * 职位申请表  前端控制器
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Api(description = "职位申请记录表")
@RestController
@RequestMapping("/qs/post/request")
public class QsPostRequestController extends AbstractController<IQsPostRequestService, QsPostRequest> {

    @ApiOperation(value = "分页")
    @GetMapping("/page")
    public R<Page> page(@Valid @ModelAttribute BaseRequest request){
        return super.page(request);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detail/{id}")
    public R<QsPostRequestDetail> detail(@PathVariable("id") Integer id){
        return new R(baseService);
    }

    @ApiOperation(value = "职位申请")
    @PostMapping("/add")
    public R<Integer> add(@RequestBody QsPostAddRequest request){
        QsPostRequest qpr = new QsPostRequest();
        BeanUtils.copyProperties(request, qpr);
        return super.add(qpr);
    }

}