package com.qinsung.admin.api.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 职位申请请求实体
 */
@Setter
@Getter
public class QsPostAddRequest {

    @ApiModelProperty(value = "职位")
    private Integer recruitId;

    @ApiModelProperty(value = "简历")
    private String curriculumVitae;
}
