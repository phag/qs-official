package com.qinsung.admin.api.common.advice;

import com.qinsung.common.base.R;
import com.qinsung.common.exception.CoreBusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 异常通知，统一为200返回
 */
@ControllerAdvice
@ResponseBody
public class ExceptionAdvice {

    private Logger logger = LoggerFactory.getLogger(ExceptionAdvice.class);

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(CoreBusinessException.class)
    public R handleException(CoreBusinessException e){
        logger.error("通用异常", e);
        R r = new R();
        r.setCode(e.getErrorCode());
        r.setMsg(e.getErrorMessage());
        return r;
    }
}
