package com.qinsung.admin.api.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Web配置类
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 配置跨域映射
     * （vue.js需在main.js的import下添加：axios.defaults.withCredentials = true）
     * （jQuery需在ajax增加配置：xhrFields:{
     *                              withCredentials:true}）
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //设置允许跨域的路径
        registry.addMapping("/**")
                //设置允许跨域请求的域名
                .allowedOrigins("*")
                //是否允许证书 不再默认开启
                .allowCredentials(true)
                //设置允许的方法
                .allowedMethods("POST", "GET", "PUT", "DELETE")
                //跨域允许时间
                .maxAge(3600);
    }
}
