package com.qinsung.admin.api.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.qinsung.admin.api.request.QsInformationPageRequest;
import com.qinsung.admin.api.request.QsInformationRequest;
import com.qinsung.common.base.R;
import com.qinsung.dal.model.entity.QsInformation;
import com.qinsung.dal.model.param.QsInformationPageParam;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.qinsung.services.module.IQsInformationService;
import com.qinsung.common.base.AbstractController;

import javax.validation.Valid;

/**
 * <p>
 * 资讯信息表  前端控制器
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Api(description = "资讯信息表")
@RestController
@RequestMapping("/qs/information")
public class QsInformationController extends AbstractController<IQsInformationService, QsInformation> {


    @ApiOperation(value = "分页")
    @GetMapping("/page")
    public R<Page> page(@Valid @ModelAttribute QsInformationPageRequest request){
        QsInformationPageParam param = new QsInformationPageParam();
        BeanUtils.copyProperties(request, param);
        return new R(baseService.queryPage(param));
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detail/{id}")
    public R<QsInformation> detail(@PathVariable("id") Integer id){
        return super.detail(id);
    }

    @ApiOperation(value = "添加")
    @PostMapping("/add")
    public R<Integer> add(@RequestBody QsInformationRequest request){
        QsInformation information = new QsInformation();
        BeanUtils.copyProperties(request, information);
        return super.add(information);
    }

    @ApiOperation(value = "编辑")
    @PostMapping("/edit")
    public R<Integer> edit(@RequestBody QsInformationRequest request){
        QsInformation information = new QsInformation();
        BeanUtils.copyProperties(request, information);
        return super.edit(information);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete/{id}")
    public R<Boolean> delete(@PathVariable("id") Integer id){
        super.logicRemove(id);
        return new R(true);
    }

}