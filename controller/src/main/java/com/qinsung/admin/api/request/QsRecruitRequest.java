package com.qinsung.admin.api.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 招聘信息请求实体
 */
@Setter
@Getter
public class QsRecruitRequest {

    private Integer id;

    @ApiModelProperty(value = "岗位")
    private String post;

    @ApiModelProperty(value = "薪资")
    private String salary;

    @ApiModelProperty(value = "福利")
    private String welfare;

    @ApiModelProperty(value = "要求")
    private String demand;

    @ApiModelProperty(value = "岗位描述")
    private String postDescription;

    @ApiModelProperty(value = "任职要求")
    private String jobRequirements;
}
