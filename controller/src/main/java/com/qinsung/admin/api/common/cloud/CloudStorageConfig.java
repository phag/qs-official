/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.qinsung.admin.api.common.cloud;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 云存储配置信息
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-03-25 16:12
 */
@Setter
@Getter
public class CloudStorageConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 阿里云绑定的域名
     */
    private String aliyunDomain = "http://yyk-upload.oss-cn-shenzhen.aliyuncs.com";
    /**
     * 阿里云路径前缀
     */
    private String aliyunPrefix = "qs-official";
    /**
     * 阿里云EndPoint
     */
    private String aliyunEndPoint = "oss-cn-shenzhen.aliyuncs.com";
    /**
     * 阿里云AccessKeyId
     */
    private String aliyunAccessKeyId = "LTAIBd76wZy6Xbvp";
    /**
     * 阿里云AccessKeySecret
     */
    private String aliyunAccessKeySecret = "c6pCf9b3cxxpyTsgpz9dzFMYWBBJfv";
    /**
     * 阿里云BucketName
     */
    private String aliyunBucketName = "yyk-upload";

}
