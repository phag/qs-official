package com.qinsung.admin.api.controller;

import com.qinsung.common.base.BaseRequest;
import com.qinsung.common.base.R;
import com.qinsung.dal.model.entity.UserInfo;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.qinsung.services.module.IUserInfoService;
import com.qinsung.common.base.AbstractController;

import javax.validation.Valid;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author GY
 * @since 2019-03-14
 */
@Api(description = "用户信息接口")
@RestController
@RequestMapping("/user/info")
public class UserInfoController extends AbstractController<IUserInfoService, UserInfo> {

    @ApiOperation(value = "分页")
    @GetMapping("/page")
    public R page(@Valid @ModelAttribute BaseRequest request) {
        return super.page(request);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detail/{id}")
    public R detail(@PathVariable("id") Integer id) {
        return super.detail(id);
    }

    @ApiOperation(value = "添加")
    @PostMapping("/add")
    public R add(@RequestBody UserInfo userInfo) {
        return super.add(userInfo);
    }

    @ApiOperation(value = "编辑")
    @PostMapping("/edit")
    public R edit(@RequestBody UserInfo userInfo) {
        return super.edit(userInfo);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete/{id}")
    public R delete(@PathVariable("id") Integer id){
        return super.logicRemove(id);
    }
}