package com.qinsung.admin.api.controller;

import com.qinsung.common.base.R;
import com.qinsung.admin.api.common.cloud.OSSFactory;
import com.qinsung.common.enums.CoreExceptionEnum;
import com.qinsung.common.exception.CoreBusinessException;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

@Api(value = "阿里云文件控制类")
@RestController
@RequestMapping("/oss")
public class OSSController {

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file, @RequestParam(value="fileType",defaultValue="sys") String fileType) throws Exception {
        if (file.isEmpty()) {
            throw CoreBusinessException.throwException(CoreExceptionEnum.FILE_IS_NULL);
        }
        //上传文件
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String url = OSSFactory.build().uploadSuffix(file.getBytes(), suffix,fileType);
        return new R(url);
    }

    @GetMapping("/download")
    public R download(@RequestParam("url") String url){
        InputStream inputStream = OSSFactory.build().downloadToInputStream(url);
        return new R(inputStream);
    }
}
