package com.qinsung.admin.api.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.qinsung.admin.api.request.QsRecruitRequest;
import com.qinsung.common.base.BaseRequest;
import com.qinsung.common.base.R;
import com.qinsung.dal.model.entity.QsRecruit;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import com.qinsung.services.module.IQsRecruitService;
import com.qinsung.common.base.AbstractController;

import javax.validation.Valid;

/**
 * <p>
 * 招聘信息表  前端控制器
 * </p>
 *
 * @author GY
 * @since 2019-03-15
 */
@Api(description = "招聘信息表")
@RestController
@RequestMapping("/qs/recruit")
public class QsRecruitController extends AbstractController<IQsRecruitService, QsRecruit> {

    @ApiOperation(value = "招聘信息列表")
    @GetMapping("/page")
    public R<Page> page(@Valid @ModelAttribute BaseRequest request){
        return super.page(request);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detail/{id}")
    public R<QsRecruit> detail(@PathVariable("id") Integer id){
        return super.detail(id);
    }

    @ApiOperation(value = "添加")
    @PostMapping("/add")
    public R<Integer> add(@RequestBody QsRecruitRequest request){
        QsRecruit recruit = new QsRecruit();
        BeanUtils.copyProperties(request, recruit);
        return super.add(recruit);
    }

    @ApiOperation(value = "编辑")
    @PostMapping("/edit")
    public R<Boolean> edit(@RequestBody QsRecruitRequest request){
        QsRecruit recruit = new QsRecruit();
        BeanUtils.copyProperties(request, recruit);
        return super.edit(recruit);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete/{id}")
    public R<Boolean> delete(@PathVariable("id") Integer id){
        return super.logicRemove(id);
    }
}